# arthurcarlsson.se
[![build status](https://gitlab.com/arthurc/arthurc.gitlab.io/badges/master/build.svg)](https://gitlab.com/arthurc/arthurc.gitlab.io/commits/master)

My blog.

## Running
```shell
$ bundle exec jekyll serve
```

## Build
```shell
$ bundle exec jekyll build # Result will be in "_site"
```

## Theme
http://mmistakes.github.io/minimal-mistakes/
